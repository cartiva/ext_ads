# coding=utf-8
"""

"""
import db_cnx
import datetime
ads_con = None
try:
    with db_cnx.ads_dpsvseries() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """select * FROM vusedcarsopenrecon;"""
            start_ts = datetime.datetime.now().strftime("%m/%d/%Y %H:%M:%S")
            ads_cur.execute(sql)
            stop_ts = datetime.datetime.now().strftime("%m/%d/%Y %H:%M:%S")
            sql = "insert into z_test_open_recon(start_ts,stop_ts) values('%s','%s')"%(start_ts,stop_ts)
            ads_cur.execute(sql)
            sql = """
                UPDATE z_test_open_recon
                SET elapsed_time = timestampdiff(sql_tsi_second, start_ts, stop_ts)
                where elapsed_time is null;
            """
            ads_cur.execute(sql)
except Exception, error:
    print error
finally:
    if ads_con:
        ads_con.close()

