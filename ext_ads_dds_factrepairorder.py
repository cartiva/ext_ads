# coding=utf-8
import db_cnx
import ops
import string
import csv

task = 'ext_ads_dds_fact_repair_order'
pg_con = None
ads_con = None
# run_id = None
file_name = 'files/ext_fact_repair_order.csv'

try:
    # run_id = ops.log_start(task)
    # if ops.dependency_check(task) != 0:
    #     ops.log_dependency_failure(run_id)
    #     'Failed dependency check'
    #     exit()
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT a.*
                from factrepairorder a
                -- inner join day b on a.opendatekey = b.datekey
                -- where b.thedate > curdate() - 730
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate ads.ext_fact_repair_order_tmp")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy ads.ext_fact_repair_order_tmp from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                delete
                from ads.ext_fact_repair_order
                where ro in (
                  select ro
                  from ads.ext_fact_repair_order_tmp);
            """
            pg_cur.execute(sql)
            sql = """
                insert into ads.ext_fact_repair_order
                select * from ads.ext_fact_repair_order_tmp
            """
            pg_cur.execute(sql)
    # ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()
