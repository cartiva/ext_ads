# coding=utf-8
import db_cnx
import ops
import csv

"""
******************************************************************************
DEPRECATED
SEE python projects/ads_for_luigi/uc_inventory.py
******************************************************************************
nightly load
"""

table_name = 'ads.tmp_ext_vehicle_items'
pg_con = None
ads_con = None
file_name = 'files/tmp_ext_vehicle_items.csv'
task = 'tmp_ext_vehicle_items'
run_id = task
try:
    with db_cnx.ads_dpsvseries() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT vehicleitemid,vin,bodystyle,trim,interiorcolor,exteriorcolor,engine,
                  transmission,make,model,yearmodel,vinresolved,cast(NULL AS sql_char)
                from vehicleitems
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """ truncate """ + table_name
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                update ads.tmp_ext_vehicle_items x
                set hash = y.hash
                from (
                  select vehicleitemid, md5(a::text) as hash
                  from (
                    select vehicleitemid,vin,bodystyle,trim,interiorcolor,exteriorcolor,engine,
                      transmission,make,model,yearmodel,vinresolved
                    from ads.tmp_ext_vehicle_items) a) y
                where x.vehicleitemid = y.vehicleitemid
            """
            pg_cur.execute(sql)
            sql = """
                insert into ads.ext_vehicle_items
                -- changed rows
                select a.vehicleitemid,a.vin,a.bodystyle,a.trim,a.interiorcolor,a.exteriorcolor,a.engine,
                  a.transmission,a.make,a.model,a.yearmodel,a.vinresolved,a.hash
                from ads.tmp_ext_vehicle_items a
                inner join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
                where a.hash <> b.hash
                union
                -- new rows
                select a.vehicleitemid,a.vin,a.bodystyle,a.trim,a.interiorcolor,a.exteriorcolor,a.engine,
                  a.transmission,a.make,a.model,a.yearmodel,a.vinresolved,a.hash
                from ads.tmp_ext_vehicle_items a
                left join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
                where b.vehicleitemid is null
                on conflict (vehicleitemid)
                do update
                set (vin,bodystyle,trim,interiorcolor,exteriorcolor,
                  engine,transmission,make,model,yearmodel,vinresolved,hash)
                = (excluded.vin,excluded.bodystyle,excluded.trim,excluded.interiorcolor,
                  excluded.exteriorcolor,excluded.engine,excluded.transmission,excluded.make,
                  excluded.model,excluded.yearmodel,excluded.vinresolved,excluded.hash);
            """
            pg_cur.execute(sql)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , TRUE
            """
            pg_cur.execute(sql)
except Exception, error:
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , FALSE
            """
            pg_cur.execute(sql)
    print error
    ops.email_error(task,run_id,error)
finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()
