# coding=utf-8
import db_cnx
import ops
import string
import csv

task = 'ext_ads_sco_tpEmployees'
pg_con = None
ads_con = None
run_id = None
file_name = 'files/ext_ads_sco_tpEmployees.csv'

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        'Failed dependency check'
        exit()
    with db_cnx.ads_sco() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT username,firstname,lastname,employeenumber,password,membergroup,storecode,fullname
                from tpemployees
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate ads.ext_sco_tpEmployees")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy ads.ext_sco_tpEmployees from stdin with csv encoding 'latin-1 '""", io)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()