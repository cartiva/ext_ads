# coding=utf-8
import db_cnx
import ops
import csv

"""
******************************************************************************
DEPRECATED
SEE python projects/ads_for_luigi/uc_inventory.py
******************************************************************************
nightly load
"""

table_name = 'ads.tmp_ext_vehicle_inventory_items'
pg_con = None
ads_con = None
file_name = 'files/tmp_ext_vehicle_inventory_items.csv'
task = 'tmp_ext_vehicle_inventory_items'
run_id = task

try:
    with db_cnx.ads_dpsvseries() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT vehicleinventoryitemid,tablekey,basistable,stocknumber,fromts,
                  coalesce(thruts, cast('12/31/9999 01:00:00' AS sql_timestamp)),
                  locationid,vehicleitemid,bookerid,currentpriority,owninglocationid,
                  cast(NULL AS sql_char)
                from vehicleinventoryitems
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """ truncate """ + table_name
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                update ads.tmp_ext_vehicle_inventory_items x
                set hash = y.hash
                from (
                  select vehicleinventoryitemid, md5(a::text) as hash
                  from (
                    select vehicleinventoryitemid,tablekey,basistable,stocknumber,fromts,thruts,
                      locationid,vehicleitemid,bookerid,currentpriority,owninglocationid
                    from ads.tmp_ext_vehicle_inventory_items) a) y
                where x.vehicleinventoryitemid = y.vehicleinventoryitemid;
            """
            pg_cur.execute(sql)
            sql = """
                insert into ads.ext_vehicle_inventory_items
                -- changed rows
                select a.vehicleinventoryitemid,a.tablekey,a.basistable,a.stocknumber,a.fromts,
                  a.thruts,a.locationid,a.vehicleitemid,a.bookerid,a.currentpriority,a.owninglocationid,a.hash
                from ads.tmp_ext_vehicle_inventory_items a
                inner join ads.ext_vehicle_inventory_items b on a.vehicleInventoryItemID = b.vehicleInventoryItemID
                where a.hash <> b.hash
                union
                -- new rows
                select a.vehicleinventoryitemid,a.tablekey,a.basistable,a.stocknumber,a.fromts,a.thruts,
                  a.locationid,a.vehicleitemid,a.bookerid,a.currentpriority,a.owninglocationid,a.hash
                from ads.tmp_ext_vehicle_inventory_items a
                left join ads.ext_vehicle_inventory_items b on a.vehicleInventoryItemID = b.vehicleInventoryItemID
                where b.vehicleInventoryItemID is null
                on conflict (vehicleInventoryItemID)
                do update
                set (tablekey,basistable,stocknumber,fromts,thruts,
                  locationid,vehicleitemid,bookerid,currentpriority,owninglocationid,hash)
                = (excluded.tablekey,excluded.basistable,excluded.stocknumber,excluded.fromts,excluded.thruts,
                  excluded.locationid,excluded.vehicleitemid,excluded.bookerid,excluded.currentpriority,
                  excluded.owninglocationid,excluded.hash);
            """
            pg_cur.execute(sql)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , TRUE
            """
            pg_cur.execute(sql)
except Exception, error:
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , FALSE
            """
            pg_cur.execute(sql)
    print error
    ops.email_error(task,run_id,error)
finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()