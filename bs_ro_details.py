# coding=utf-8
import db_cnx
import csv
"""
10/27/16: added payment type code, vin
"""
pg_con = None
ads_con = None
file_name = 'files/bs_ro_details.csv'
file_name_1 = 'files/bs_tech_proficiency.csv'
try:
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT bbb.thedate, b.thedate as close_date, a.ro , a.line, a.flaghours, bb.thedate AS flag_date,
                  d.complaint, d.cause, d.correction, e.opcode, left(trim(e.description), 100) AS opcode_desc,
                  f.opcode as corcode, left(trim(f.description), 100) AS corcode_desc,
                  g.comment,a.ropartssales, a.rolaborsales,
                  a.roshopsupplies, a.rohazardousmaterials, a.ropaintmaterials,
                  h.technumber, coalesce(h.employeenumber, 'NA'), coalesce(h.laborcost, 0), h.flagDeptCode,
                  coalesce(i.lastname, 'N/A'), coalesce(i.firstname, 'N/A'),
                  b.biweeklypayperiodstartdate, b.biweeklypayperiodenddate, j.paymenttypecode, k.vin
                --INTO #bs_ro_details
                -- select a.*
                FROM factrepairorder a
                INNER JOIN day b on a.finalclosedatekey = b.datekey
                --  AND b.yearmonth BETWEEN 201508 AND 201607
                  AND b.thedate BETWEEN (
                      SELECT min(biweeklypayperiodstartdate)
                      FROM day
                      WHERE yearmonth = 201509)
                    AND (
                      SELECT max(biweeklypayperiodenddate)
                      FROM day
                      WHERE yearmonth = 201608)
                LEFT JOIN day bb on a.flagdatekey = bb.datekey
                left join day bbb on a.opendatekey = bbb.datekey
                INNER JOIN dimservicetype c on a.servicetypekey =  c.servicetypekey
                  AND c.servicetypecode = 'BS'
                INNER JOIN dimccc d on a.ccckey = d.ccckey
                INNER JOIN dimopcode e on a.opcodekey = e.opcodekey
                  AND e.opcode NOT IN ('SHOP','TRAIN')
                INNER JOIN dimopcode f on a.corcodekey = f.opcodekey
                INNER JOIN dimrocomment g on a.rocommentkey = g.rocommentkey
                -- INNER JOIN dimtech h on a.techkey = h.techkey
                --   AND h.flagdeptcode = 'BS'
                --   AND h.employeenumber <> 'NA'
                LEFT JOIN dimtech h on a.techkey = h.techkey
                LEFT JOIN edwEmployeeDim i on h.employeenumber = i.employeenumber
                  AND i.currentrow = true
                -- WHERE roflaghours <> 0
                left join dimPaymentType j on a.paymentTypeKey = j.paymentTypeKey
                left join dimVehicle k on a.vehiclekey = k.vehiclekey
                WHERE left(a.ro,2) = '18'
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate ads.bs_ro_details")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy ads.bs_ro_details from stdin with csv encoding 'latin-1 '""", io)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    print error
finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()
