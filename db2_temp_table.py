# coding=utf-8
import db_cnx
# import string
import csv

arkona_con = None
ads_con = None
run_id = None
file_name = 'files/bad_service_types.csv'
file_name_1 = 'files/new_service_types.csv'

try:
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT ro, line
                FROM factrepairorder
                WHERE servicetypekey = 1
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
        with db_cnx.arkona_report() as arkona_con:
            with arkona_con.cursor() as arkona_cur:
                sql = """
                    DECLARE GLOBAL TEMPORARY TABLE  SESSION.TEMP_SERVICE_TYPES (
                      RO  CHAR(8),
                      LINE INTEGER)
                """
                arkona_cur.execute(sql)
                arkona_con.commit()
                csv_data = csv.reader(file(file_name))
                for row in csv_data:
                    sql = """
                            insert into SESSION.TEMP_SERVICE_TYPES values (%s,%s)
                        """ % ("'" + row[0] + "'", row[1])
                    arkona_cur.execute(sql)
                    arkona_con.commit()
                sql = """
                    select trim(b.ptro#), b.ptline, trim(b.ptsvctyp)
                    from session.temp_service_types a
                    inner join rydedata.SDPRDET b on a.ro = trim(b.ptro#)
                      and a.line = b.ptline
                      and b.ptltyp = 'A'
                """
                arkona_cur.execute(sql)
                with open(file_name_1, 'wb') as f:
                    csv.writer(f).writerows(arkona_cur.fetchall())
                csv_data = csv.reader(file(file_name_1))
                for row in csv_data:
                    sql = """
                        insert into z_fix_dim_service_type
                        values ('%s',%s,'%s')
                    """ % (row[0], row[1], row[2])
                    ads_cur.execute(sql)
                ads_con.commit()
                sql = """
                    select trim(a.document_number), b.ptline, trim(b.ptsvctyp)
                    from rydedata.pdpphdr a
                    inner join rydedata.pdppdet b on a.ptpkey = b.ptpkey
                    inner join SESSION.TEMP_SERVICE_TYPES aa on trim(a.document_number) = trim(aa.ro)
                      and b.ptline = aa.line
                    where a.document_type = 'RO'
                      and b.ptltyp = 'A'
                    group by trim(a.document_number), b.ptline, trim(b.ptsvctyp)
                """
                arkona_cur.execute(sql)
                with open(file_name_1, 'wb') as f:
                    csv.writer(f).writerows(arkona_cur.fetchall())
                csv_data = csv.reader(file(file_name_1))
                for row in csv_data:
                    sql = """
                        insert into z_fix_dim_service_type
                        values ('%s',%s,'%s')
                    """ % (row[0], row[1], row[2])
                    ads_cur.execute(sql)
                ads_con.commit()
            # for row in ads_cur.fetchall():
            #     sql = """
            #         insert into SESSION.TEMP_SERVICE_TYPES values (%s,%s)
            #     """ % ("'" + row[0] + "'", row[1])
            #     print sql
            #     arkona_cur.execute(sql)
            #     arkona_con.commit()
            # sql = """
            #     select * from SESSION.TEMP_SERVICE_TYPES
            # """
            # arkona_cur.execute(sql)
            # for row in arkona_cur.fetchall():
            #     print str(row)
                    # for row in ads_cur.fetchall():
                    #     sql = """
                    #         select trim(ptro#), ptline, trim(ptsvctyp)
                    #         from rydedata.sdprdet
                    #         where trim(ptro#) = '%s'
                    #           and ptline = %s
                    #           and ptltyp = 'A'
                    #           and ptro# is not null
                    #     """ % (row.ro, row.line)
                    #     arkona_cur.execute(sql)
                    #     for new_row in arkona_cur.fetchall():
                    #         with ads_con.cursor() as ads_insert_cur:
                    #             # a = "'" + new_row[0] + "'"
                    #             # b = new_row[1]
                    #             # c = "'" + new_row[2] + "'"
                    #             sql = """
                    #                 insert into z_fix_dim_service_type
                    #                 values (%s,%s,%s)
                    #             """ % ("'" + new_row[0] + "'", new_row[1], "'" + new_row[2] + "'")
                    #             print sql
                    #             ads_insert_cur.execute(sql)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    print error
finally:
    if ads_con:
        ads_con.close()
    if arkona_con:
        arkona_con.close()
