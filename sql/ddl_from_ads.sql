DECLARE @str memo;
DECLARE @crlf string;
DECLARE @tableName string;
DECLARE @columnName string;
DECLARE @lastField integer;
DECLARE @i integer;
DECLARE @schema string;
DECLARE @hasPk string;
DECLARE @tableCur cursor AS
  SELECT LEFT(name, 50) AS tableName,
    CASE WHEN table_primary_key IS NULL THEN 'false' ELSE 'true' END AS hasPk
  FROM system.tables
  -- SET the TABLE name -------------------------------------------------------
  WHERE name = 'vehicleinventoryitems';
DECLARE @columnCur CURSOR AS
  SELECT name, field_num,
    CASE field_type
  	  WHEN 1 THEN 'boolean' --logical
-- simulate bigint IN ads with numeric(20,0)
      WHEN 2 THEN -- numeric
        CASE
          WHEN field_length = 20 AND field_decimal = 0 THEN 'bigint'
          ELSE 'numeric (' + trim(CAST(field_length AS sql_char))
            + ',' + trim(CAST(field_decimal as sql_char)) + ')'
        END
  	  WHEN 3 THEN  'date' -- date
  	  WHEN 4 THEN 'citext' -- char
  	  WHEN 5 THEN 'citext' -- memo
      WHEN 6 THEN 'citext' -- blob any -- for now, NOT sure what will WORK
      WHEN 7 THEN 'citext' -- blob bitmap -- for now, NOT sure what will WORK
      WHEN 8 THEN 'citext' -- varchar
      WHEN 10 THEN -- double -- IN ads, default double results IN (8,15)
        CASE
          WHEN field_length = 8 AND field_decimal = 15 THEN 'numeric (12,4)'
          ELSE 'numeric (' + trim(CAST(field_length AS sql_char))
          + ',' + trim(CAST(field_decimal as sql_char)) + ')'
        END
  	  WHEN 11 THEN 'integer' -- integer
      WHEN 12 THEN 'smallint' -- shortint signed short integer
      WHEN 13 THEN 'time without time zone' -- time
  	  WHEN 14 THEN 'timestamp with time zone' -- timestamp
      WHEN 15 THEN 'serial' -- autoinc   ** TRY serial, don't know how this will WORK **
      WHEN 17 THEN 'numeric(12,4)' -- curdouble
      WHEN 18 THEN 'numeric(12,4)' -- money
  	  WHEN 20 THEN 'citext' -- cichar
      WHEN 22 THEN 'timestamp with time zone' -- modtime
      WHEN 23 THEN 'citext' -- Visual FoxPro varchar field
  	  ELSE 'xxxx'
	  END AS field_type,
    field_type as adsFieldType,
	  field_Length,
    field_decimal, field_can_be_null,
    field_default_value
--    CASE WHEN field_default_value IS NULL THEN 'false' ELSE 'true' END AS hasDefaultValue
  FROM system.columns
  WHERE parent = @tableName;
DECLARE @lastFieldCur CURSOR AS
  SELECT MAX(field_num) AS lastField
  FROM system.columns
  WHERE parent = @tableName;
DECLARE @pkCur CURSOR AS
  SELECT b.index_expression
  FROM system.tables a
  LEFT JOIN system.indexes b on a.name = b.parent
    AND a.table_primary_key = b.name
  WHERE a.name = @tableName;
@crlf = Char(13) + Char(10);
@str = '';
-- SET the schema -------------------------------------------------------------
@schema = 'ads';
OPEN @tableCur;
TRY
  WHILE FETCH @tableCur DO
    @tableName = @tableCur.tableName;
    @hasPk = @tableCur.hasPk;
    @i = 1;
    OPEN @lastFieldCur;
    TRY
      WHILE FETCH @lastFieldCur DO
        @lastField = @lastFieldCur.lastField;
      END WHILE;
    FINALLY
      CLOSE @lastFieldCur;
    END TRY;
	  -- @str = @str + 'create TABLE ' + @schema + '.sco_' + @tableName + '(' + @crlf;
    @str = @str + 'create TABLE ' + @schema + '.' + @tableName + '(' + @crlf;
  	OPEN @columnCur;
  	TRY
  	  WHILE FETCH @columnCur DO
        @columnName = replace(@columnCur.name, '#', '_');
        IF @lastField = 1 THEN -- single COLUMN tables
          @str = @str + '    ' +  @columnName + ' ' +  @columnCur.field_type
            + iif(@columnCur.field_can_be_null = true, '',' NOT NULL')
            + iif(CAST(@columnCur.field_default_value AS sql_char) IS NULL,'',' DEFAULT '
            + iif(@columnCur.adsFieldType IN (3,4,5,6,7,8,13,14,20,22,23), '''' + @columnCur.field_default_value + '''', @columnCur.field_default_value));
          IF @hasPk = 'false' THEN
            @str = @str + ')' + @crlf;
          ELSE -- has pk
            OPEN @pkCur;
            TRY
              WHILE FETCH @pkCur DO
                @str = @str + ',' + @crlf + 'PRIMARY KEY (' + replace(@pkCur.index_expression,';',',') + '))';
              END WHILE;
            FINALLY
              CLOSE @pkCur;
            END TRY;
          END IF; -- has pk
        ELSEIF @i < @lastField THEN  -- not the last field, END line with comma
  		    @str = @str + '    ' + @columnName + ' ' +  @columnCur.field_type
            + iif(@columnCur.field_can_be_null = true, '',' NOT NULL')
            + iif(CAST(@columnCur.field_default_value AS sql_char) IS NULL,'',' DEFAULT '
            + iif(@columnCur.adsFieldType IN (3,4,5,6,7,8,13,14,20,22,23), '''' + @columnCur.field_default_value + '''', @columnCur.field_default_value))
            + ',' + @crlf;
          @i = @i + 1;
        ELSE -- this is the last field, END line with right paren
          IF @hasPk = 'false' THEN
            @str = @str + '    ' +  @columnName + ' ' +  @columnCur.field_type
            + iif(CAST(@columnCur.field_default_value AS sql_char) IS NULL,'',' DEFAULT '
            + iif(@columnCur.adsFieldType IN (3,4,5,6,7,8,13,14,20,22,23), '''' + @columnCur.field_default_value + '''', @columnCur.field_default_value))
            + ')' + @crlf;
          ELSE -- has pk
            OPEN @pkCur;
            TRY
              WHILE FETCH @pkCur DO
                @str = @str + '    ' +  @columnName + ' ' +  @columnCur.field_type
                + iif(CAST(@columnCur.field_default_value AS sql_char) IS NULL,'',' DEFAULT '
                + iif(@columnCur.adsFieldType IN (3,4,5,6,7,8,13,14,20,22,23), '''' + @columnCur.field_default_value + '''', @columnCur.field_default_value))
                + ',' + @crlf;
                @str = @str + '    ' + 'PRIMARY KEY (' + replace(replace(@pkCur.index_expression,';',','), '#', '_') + '))';
              END WHILE;
            FINALLY
              CLOSE @pkCur;
            END TRY;
          END IF; -- has pk
        END IF; -- last field
  	  END WHILE; -- WHILE FETCH @columnCur DO
  	FINALLY
  	  CLOSE @columnCur;
  	END TRY;
    @str = @str + ' WITH (OIDS=FALSE);' + @crlf + @crlf;
  END WHILE; --FETCH @tableCur DO
FINALLY
  CLOSE @tableCur;
END TRY;


SELECT @str FROM system.iota;


