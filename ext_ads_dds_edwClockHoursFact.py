# coding=utf-8
import db_cnx
import csv

task = 'ext_edw_clock_hours_fact'
pg_con = None
ads_con = None
run_id = None
file_name = 'files/ext_edw_clock_hours_fact.csv'

try:
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT employeekey,datekey,clockhours,regularhours,overtimehours,vacationhours,
                  ptohours,holidayhours
                from edwClockHoursFact
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate ads.ext_edw_clock_hours_fact")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy ads.ext_edw_clock_hours_fact from stdin
                with csv encoding 'latin-1 '""", io)
    # ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()
