# coding=utf-8
import db_cnx
import csv

pg_con = None
ads_con = None
file_name = 'files/repair_order.csv'
try:
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT a.storecode, b.thedate, a.ro, a.line, c.servicetypecode, d.paymenttype
                FROM factrepairorder a
                INNER JOIN day b on a.finalclosedatekey = b.datekey
                INNER JOIN dimservicetype c on a.servicetypekey = c.servicetypekey
                INNER JOIN dimpaymenttype d on a.paymenttypekey = d.paymenttypekey
                WHERE b.thedate BETWEEN '10/01/2015' AND '09/30/2016'
                  AND storecode = 'ry1'
                group by a.storecode, b.thedate, a.ro, a.line, c.servicetypecode, d.paymenttype
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate ads.repair_order")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy ads.repair_order from stdin with csv encoding 'latin-1 '""", io)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    print error
finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()
